# calculator_parser

C++ library to perform string parsing for a calculator.

This project is provided with a Visual Studio 2019 solution that has the calculator shell/command line tool and unit tests.

# Design

UML is used to represent the design of the calculator parser.

The project provides the yEd UML file `calculator.graphml` and a PNG file for viewing in the browser.

The UML for the calculator parser uses [yEd](https://www.yworks.com/products/yed) UML desktop editor.

# License

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

# Project status

Project is currently under development.
